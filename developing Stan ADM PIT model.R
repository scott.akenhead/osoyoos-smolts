# developing Stan ADM PIT model.R
years=4; segments=5; runs=10;
advect= matrix(nrow=years, ncol=runs+segments);
LPA= matrix(1:20, nrow=years, ncol=segments, byrow=T);
river= c(1:5);
for (year in 1:1){
    for(seg in 1:segments) advect[year, seg]= river[seg]*LPA[year, seg];
    seg=segments;
    for(j in 1:(segments-1)) {            # start of run, 1 to 4
        for(k in (j+1):segments){          # end   of run, 2 to 5, 3 to 5, 4 to 5,etc
            seg=seg+1 ;
            cat(seg, j,k,"\n");
            cat(advect[year,j:k],"\n");
            advect[year,seg] = sum(advect[year,j:k])
        }
    }
}
# ==========

Taylor_lpdf= function(t, advect,diff,mort){
    # t is data that the model will be fitted to.
    # t is an integer vector. Used as index into tabled function.

    # diff is eddy diffusivity, variance increases with time.
    # Taylor equation:
    # p =  (1/sqrt(4*diff*t)) * exp(-(t-advect)^2/(4*diff*t))
    # -1.265512123 is -.5*log(4*pi) is  log(1/sqrt(4*pi))
# 1. build entire log density function from 1 to max(t).  Prob(t=0)=0
    logpx=numeric(100) # allocate vector for tabled function.
    logp=1.0
    j=0 # index into table
    x=0.0 # elapsed time for Taylor(AD) and for daily mortality
    while(x < advect | logp > -13.816){   # end right side when prob=1/million
        x=x+1.0
        logp= (-1.265512123 -0.5*log(diff*x)) -(x-advect)^2 /(4*diff*x); # log Taylor
        j=j+1
        logpx[j]=logp
    }
cat("j =",j, "logpx[1:20] =", logpx[1:20],"\n")
#    if (logpx[30] > -6.9 ) stop("error in Taylor_lpdf: pr(x=30) > 0.001")
    logpx[1:j] = logpx[1:j] - mort*c(1.0 : j)  # survival declines with elapsed time, as logs
    survived = sum(exp(logpx[1:j])) # how many survived?
    # cat("survived =",survived,"\n")
    logpx[1:j]=logpx[1:j] - log(survived) # so sum(probabilities)=1
# 2. look up log probs of the data
    #if(any(t > j)) cat("j =",j," but some t =", t[t>j],"\n")
    # x=logpx[t] # if t is large, logpx(t>j) = 0, no effect on NLL fitting
    x= approx(c(1:j),logpx[1:j],t,yleft=0,yright=0);
    return(x);
}
t=seq(1,10.,.5); advect=5; diff=0.3; mort=0.2;
a=Taylor_lpdf(t, advect,diff,mort)
SetPar()
plot(1,1,type="n",xlim=c(0,20), ylim=c(0,.5))
x=1:20
p =  (1/sqrt(4*pi*diff*x)) * exp(-(x-advect)^2/(4*diff*x))
sum(p)
lines(x,p)
m=exp(-mort*x);
lines(x,m)
surv=p*m
lines(x,surv/sum(surv),col="red")
lines(t,exp(a$y), col="blue")

