# Stan_FFT_1.stan
functions{ 
  real Taylor_lpdf(real t, real A, real K) { # no vectors or arrays
       real x;
       x =  -(1.26551212 + 0.5*log(t*K) + (t-A)^2 /(4*t*K) );
       return x;
  }
}
data {
    int    years;
    int    segments;
    int    nobs [years, segments];  # integer array
    real   ftt [years, segments,300];
    matrix [years, segments] TPA;
    matrix [6,2] p;
 }
 parameters {
    vector <lower = 0> [5] river;
    real   <lower = 0> K;
 }
 model {
    int nob;
    river ~ normal(p[1:5,1], p[1:5,2]);
    K ~  normal(p[  6,1], p[  6,2]);
    for(year in 1:years) { 
        for (seg in 1:segments) {
            nob = nobs[year,seg];
            if(nob < 1) continue; # skip to next seg 
            for(obs in 1:nob) {
                ftt[year,seg,obs] ~ Taylor(river[seg]*TPA[year,seg], K);
            }
        }
    }
}
