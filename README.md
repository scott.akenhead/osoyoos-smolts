Osoyoos Smolts
author: Scott Akenhead, phone: 1-250-210-4410, email: scott@s4s.com

This is a modeling and statistics project funded by and reporting to Dr. Kim Hyatt at the Pacific Biological Station, Fisheries and Ocean Canada. It deals with the abundance, travel times, and mortality of sockeye salmon smolts from Osoyoos Lake, but also sockeye smolts from USA lakes in the Columbia River Watershed, as they transit 10 dams and two river segments (Okanogan River, Hantford Reach in the Columbia) en route to the Pacific Ocean at Astoria WA.  The overall goal is daily abundance and quality of smolts at ocean entry.

Respectful collaborators: Welcome! 
