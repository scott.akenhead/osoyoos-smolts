# StanFFT2.stan
functions{ 
  real Taylor_lpdf(real t, real A, real K) { # no vectors or arrays
       real x;
       x =  -(1.26551212 + 0.5*log(t*K) + (t-A)^2 /(4*t*K) );
       return x;
  }
}
data{
    int years;           
    int segments;                   # shortest: ZR, RM, MJ, JB, BT
    int  nftt [years, 15];          # integer array, count obs of ftt
    real  ftt [years, 15, 300];     # years, runs, obs. units are hours
    matrix [years, segments] TPA;   # segments only, length over flow, "time per area"
    matrix [6,2] p;                 # prior knowledge as means and std.dev 
}
parameters{
    vector <lower = 0> [5] river;   # convert TPA to FTT.
    real   <lower = 0> K;           # dispersion, increases with FTT 
}
model{
    int nob; int run;
    matrix [years, 15] advect;  
# priors
    river ~ normal(p[1:5,1], p[1:5,2]);
    K     ~ normal(p[6,  1], p[  6,2]);
# transform parameters, advect is (length/flow)*river
    for (year in 1:years){
        for(seg in 1:segments){     # first five
            advect[year, seg] = river[seg]*TPA[year,seg];
        }
        run = segments;            # ten runs, array indices 6 to 15
        # runs of contiguous segments: ZM,ZJ,ZB,ZT, RJ,RB,RT, MB,MT, JT
        for(j in 1:(segments-1)){            # start of run, 1:4
            for(k in (j+1):segments){            # end of run, 2:5, 3:5, 4:5, 5
                run = run+1 ;                # place for this sum of segments
                advect[year,run] = sum(advect[year,j:k]);
            }
        }
    } # year
# likelihood
    for(year in 1:years){
        for(seg in 1:15){
            nob = nftt[year,seg];
            if(nob < 1) continue; # skip to next seg 
            for(ob in 1:nob) {
                ftt[year,seg,ob]~Taylor(advect[year,seg],K);
            }
        }
    }
}
