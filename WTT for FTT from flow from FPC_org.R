# WTT for FTT from flow from FPC_org.R
# ---- SETUP ----
options(digits=5,show.error.locations=T,stringsAsFactors=F,show.signif.stars=F)
# -- EXTERNAL FUNCTIONS --
source('~/Documents/R_main/rScott/R/Simple.R', encoding = 'UTF-8')
# --- LOCAL FUNCTIONS ---
SetPar=function() par(
    oma=c(2,2,2,1),    # room for marginal titles, 2 lines
    mar = c(2,2.5,1,0),  # spacing between plots
    mgp = c(1,0,0),    # axis numbers close to axis
    tcl = 0.2,         # ticks inside
    xaxs = "i",yaxs = "i" # needs xlim, ylim
);

Clr=rgb(0,50,50,30, maxColorValue = 100) # semi-transparent blue-green-grey.

Margins=function(a,fnc=sum,...){  # adds right-hand col and bottom row
    a=cbind(a,apply(a,1,fnc,...));
    a=rbind(a,apply(a,2,fnc,...)); return(a);
}
# READ
folder="/Users/Scott/Documents/Projects/DFO - OK/OK Data from R";
file1="RRH_flow_spill.Rdata"
file2="MCN_flow_spill.Rdata"
file3="BON_flow_spill.Rdata"
for(file in c(file1,file2,file3)) load( file=paste(sep="/",folder,file));
dams_data = c("RRH_flow_spill","MCN_flow_spill","BON_flow_spill")
dams=substr(dams_data,1,3) #"RRH" "MCN" "BON"
years = 2004:2016
meanFlowSpill= array(dim=c(3, 13, 2, 8))
# 3 dams; 13 years; 2 params: flow, pctSpill; 8 stats: m,s,mad,min,q1,q2,q3,max
for(j in 1:3){
    a = get(dams_data[j]);
    a=a[!is.na(a$total), c("year","doy","total","spill") ]
    for (k in 1:length(years)) { # 2004 to 2016 is 13 years
        m= (a$year == years[k]) & (a$doy > 119) & (a$doy < 141) # 21 days, doy 120-140
        flow=a[m,"total"]
        meanFlowSpill[j,k,1,]=Simple(flow,do.print=F)[c(2,3,7,8,9,6,10,11)]
        pctSpill=a[m,"spill"]/flow
        meanFlowSpill[j,k,2,]=Simple(pctSpill,do.print=F)[c(2,3,7,8,9,6,10,11)]
    }}


save(meanFlowSpill, file=paste(sep="/",folder,"meanFlowSpill.Rdata") );


resultsDir="/Users/Scott/Documents/Projects/DFO - OK/OK Results/Results May 2017"
# for(dam in 1:3){ for(year in 1:13){
#     write.table(meanFlowSpill[dam,year,,],
#     file =paste(sep="/",resultsDir,"meanFlowSpill1.txt"), append=TRUE,col.names=F);
# }}
#
# -- PLOT --
ylim=c(0.0,1.05*max(meanFlowSpill[,,1,8])); # max of daily max was 13085 m^3 s^-1 in 2012
md=as.matrix(meanFlowSpill[,,1,6]); md # mean flow days 120-140 for 13 years by 3 dams
ylab=expression(paste("Flow (", m^{3}," ",s^{-1},")" ))
SetPar()
plot(1,1, ylim=ylim, xlim=c(2002,2018), ylab=ylab, xlab="Year");
axis(3,labels=F);axis(4,labels=F); # tics only, top and right.
colr=c("red","blue","black")
for(dam in 1:3) {
    lines(x=2004:2016, y=md[dam,], col=colr[dam], lwd=2);
    for (year in 1:13){
        segments(year+2003, meanFlowSpill[dam,year,1,5],
                 year+2003, meanFlowSpill[dam,year,1,7],col=colr[dam])
        points(rep(year+2003,2),c(meanFlowSpill[dam,year,1,4],
                                  meanFlowSpill[dam,year,1,8]),
            col=colr[dam], pch=20,cex=0.5)
    }
}
r2=cor(t(m))^2; round(100*r2,1);
#       RRH   MCN BON
# RRH 100.0  86.2  84.2
# MCN  86.2 100.0  98.4
# BON  84.2  98.4 100.0
>
