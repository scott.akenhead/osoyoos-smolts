# ADS advection diffusion storage.R
# Scott Akenhead 1-250-210-4410 scott@s4s.com 2017 March 30

# 0. R Options
    options(digits=4,show.error.locations=T,stringsAsFactors=F,show.signif.stars=F)
    par(
        oma=c(2,2,1,1),    # room for marginal titles, 2 lines
        mar = c(2,2,0,0),  # spacing between plots
        mgp = c(1,0,0),    # axis numbers close to axis
        tcl = 0.2,         # ticks inside
        xaxs = "i",yaxs = "i" # axis is data range (needs xlim, ylim)
    );
    # par(mfcol = c(3,1)); # plots per figure

# 1. local functions
Convolve = function(y, conv, y_tol=1e-9) {
        ny = length(y);
        nc = length(conv);
        y_out = numeric(ny + nc); # zeros
        for (j in 1:ny) {
            if (y[j] < y_tol) next;
            for (k in 1:nc) {
                # first element of conv must result in first output
                y_out[j + (k-1)] = y_out[j + (k-1)] + y[j] * conv[k];
            }
        }
        return(y_out[1:ny]); # this will lose straggler smolts
}

FilledStep <- function(x,y,colr=NULL){
    xp=c(rep(x,each=2),0);
    yp=c(0,rep(y,each=2));
    #rgb(t(col2rgb("skyblue")), alpha=125,maxColorValue=255)# "#87CEEB7D"
    #rgb(t(col2rgb("tan1")), alpha=125,maxColorValue=255)# "#FFA54F7D"
    if(is.null(colr)) colr= "#87CEEB7D" # skyblue, half density.
    polygon(x=xp,y=yp, col=colr)
}
DOY=function(day,month,year=NULL){
    if(month < 1 || month > 12) stop("DOY: month is wrong")
    if(day < 1 || day > 31) stop("DOY: day is wrong")
    doy=day+ c(0,31,59,90,120,151,181,212,243,273,304,334)[month]
    #cumsum(c(0,31,28,31,30,31,30,31,31,30,31,30,31))
    if(is.null(year))return(doy)
    if((year %% 4 == 0) && month > 2) return(doy+1)
    return(doy)
}

# 2 load data
  # 2.1 smolt passage index. cleaned and interpolated
    load("/Users/Scott/Documents/Projects/DFO - OK/OK Data from R/dat1.Rdata")
    y = 1e-3*dat1$smolts[2, 9,] # MCN 2012
  # 2.2 Water Travel Time, days 90 to 180 for 9 years
    load("/Users/Scott/Documents/Projects/DFO - OK/OK Data from R/WTT2004-2012-181edited.Rdata")
    wtt= WTT$MCNBON[ (8*181+1):(9*181) ] # 2012 April 1 to July 31

# 3. trial parameters
    advect  = 4   # days
    absorb  = 0.2 #  proportion exchanged daily between mainstream and storage
    absorb.r= log(1-absorb)  # as an exponential rate, a negative value
    mort    = 0.10  # mortality, % per day
    diffuse = 0.2   # time-dependent diffusion, standard deviation;
    days=0:90;

# 4. convolutions
    t_conv=0:60 # length 16, starts at day zero.
  # 4.1 mainstem advection with eddy diffusion, G.I. Taylor (1954)
    ad_conv = dnorm(t_conv, advect, diffuse*t_conv)
    ad_conv = ad_conv/sum(ad_conv)      # convolution conserves mass
  # 4.2 desorb out of mainstream or out of storage
    out_conv = exp(absorb.r*t_conv) # remaining in mainstream, does not sum to 1
    # 1.00 0.80 0.64 0.512 0.4096 0.3277 0.2621 0.2097 0.1678 0.1342 0.1074
    sum(out_conv) # 5 ... does not sum to 1
  # 4.3 absorb into storage
    in_conv = c(0,-diff(out_conv)) # ensure start is day zero.
    # 0.00 0.20 0.16 0.128 0.1024 0.08192 0.06554 0.05243 0.04194 0.03355
    # sum(in_conv) # 1, convolution length 60 conserves mass to ~1.5 e-6
  # 4.4 absorb and desorb as a single convolution, what comes out of storage
    st_conv=Convolve(in_conv,in_conv)
    # 0.00 0.00 0.040 0.064 0.0768 0.08192 0.08192 0.07864 0.07340 0.06711
  # 4.5 daily mortality; cumulative effect, a multiplier not a convolution.
    m_eff=exp(log(1-mort)*days) # starts day zero

# 5. test with example
    par(mfcol=c(3,1))
  # 5.1 piece by piece
  # plot (A) absorb into storage
    y0 = c(100,rep(0,90)) # length 91, smolts on day 0 (not 1)
    y_in = Convolve(y0,in_conv)  # move into storage by time not distance
    # 0. 20. 16. 12.8 10.24 8.192  6.554  5.243  4.194  3.355
    y_flow = Convolve(y,out_conv); # in flow, from day zero, not yet stored
    # 100. 80. 64. 51.2 40.96 32.77  26.21  20.97  16.78  13.42
    # "#FF000059" is red 35%
    plot(1,1,type="n",ylim=c(0,110),xlim=c(0,30),xlab="",ylab="")
    legend(x=25,y=110,legend="A",bty="n")
    FilledStep(days,y_in, colr="#00FF007F") # starts day 1
    FilledStep(days,y_flow, colr="#0000FF59") # starts day 0
  # plot (B) desorb from storage
    plot(1,1,type="n",ylim=c(0,110),xlim=c(0,30),xlab="",ylab="")
    legend(x=25,y=110,legend="B",bty="n")
    y_out=Convolve(y_in,in_conv); # starts day 1, length 90
    # 0.0 0.0 4.0 6.4 7.68 8.192 8.192 7.864 7.340 6.711 6.040
    FilledStep(days,y_out, colr="#FF000059")
    y_flow2= y_flow+y_out; #smolts in flow from never stored and desorbed
    lines(days,y_flow2, type="s")
  # plot (C) balance of absorb - desorb
    plot(1,1,type="n",ylim=c(-20,30),xlim=c(0,30),xlab="",ylab="")
    abline(h=0, lty="dotted")
    legend(x=25,y=110,legend="C",bty="n")
    lines(days,y_in-y_out,type="s", lwd=2)
    mtext("Days",  1, out=T)
    mtext("Smolts",2, out=T)

  # 5.2 absorb and desorb as a single convolution
    plot(t_conv,st_conv,type="l", xlim=c(0,30),ylim=c(0,.10)); # desorbing
    # determin smolts desorbed then re- absorbed, stored a second time.
    y_out=Convolve(y,st_conv) # same as previous

  # 5.3  second time into storage, applied to desorbed smolts
    y_in2=Convolve(y_out,in_conv)
    y_out2=Convolve(y_in2,in_conv)
    y_flow3=
    par(mfcol=c(4,1))
    plot(1,1,type="n", xlim=c(0,30),ylim=c(0,25), xlab="", ylab="");
    FilledStep(days,y_in, colr="#00FF0059")
    plot(1,1,type="n", xlim=c(0,30),ylim=c(0,25), xlab="", ylab="");
    FilledStep(days,y_out, colr="#FF000059")
    plot(1,1,type="n", xlim=c(0,30),ylim=c(0,25), xlab="", ylab="");
    FilledStep(days,y_in2, colr="#0000FF59")
    plot(1,1,type="n", xlim=c(0,30),ylim=c(0,25), xlab="", ylab="");
    FilledStep(days,y_out2, colr="#FF00FF59")
    mtext("Days",  1, out=T)
    mtext("Smolts",2, out=T)

  # 5.4  matrix to build intuition and check convolutions
    # col 1 is days(=rows) down maintream; cols 2+ are storage
    n=40;    y=matrix(ncol=n,nrow=n)
    # rows= days in flow, columns= days in storage
    for(j in 1:n) y[j,]= 100*exp(absorb.r*( (j-1):(j+(n-2))))
    y[1:5,1:5]; # y[(n-5):n,(n-5):n]
    #        [,1]  [,2]  [,3]  [,4]  [,5]
    # [1,] 100.00 80.00 64.00 51.20 40.96
    # [2,]  80.00 64.00 51.20 40.96 32.77
    # [3,]  64.00 51.20 40.96 32.77 26.21
    # [4,]  51.20 40.96 32.77 26.21 20.97
    # [5,]  40.96 32.77 26.21 20.97 16.78
    # difference along each row. diff() works by column but y=y'
    y1=-diff(y); # n-1 rows
    y1=rbind( rep(0.0,n),y1); y1[1:5,1:5] # y1 is smolts in storage.
    #       [,1]   [,2]   [,3]   [,4]  [,5]
    # [1,]  0.00  0.000  0.000  0.000 0.000
    # [2,] 20.00 16.000 12.800 10.240 8.192
    # [3,] 16.00 12.800 10.240  8.192 6.554
    # [4,] 12.80 10.240  8.192  6.554 5.243
    # [5,] 10.24  8.192  6.554  5.243 4.194
    # determine smolts re-emerging, desorbing.
    y2=-diff(y1[2:n,]); dim(y2); # 28 by 30
    y2=rbind( matrix(0.,nrow=2,ncol=n),y2) # reemerge day 2 not 0 or 1.
    y2[1:7,1:7]
    #       [,1]  [,2]  [,3]   [,4]   [,5]   [,6]   [,7]
    # [1,] 0.000 0.000 0.000 0.0000 0.0000 0.0000 0.0000
    # [2,] 0.000 0.000 0.000 0.0000 0.0000 0.0000 0.0000
    # [3,] 4.000 3.200 2.560 2.0480 1.6384 1.3107 1.0486
    # [4,] 3.200 2.560 2.048 1.6384 1.3107 1.0486 0.8389
    # [5,] 2.560 2.048 1.638 1.3107 1.0486 0.8389 0.6711
    # [6,] 2.048 1.638 1.311 1.0486 0.8389 0.6711 0.5369
    # [7,] 1.638 1.311 1.049 0.8389 0.6711 0.5369 0.4295
    # number desorbing is sum of diagonals, upper right to lower left.
    # sum columns after shifting rows to the right.
    # note: rep(x,0) is "nothing" == numeric(0) == harmless == handy
    # so row 1not shifted, row 2 shifted 1, row 3 shifted 2, ...
    y3=0.*y2;
    for(jr in 1:n) {
        y3[jr,] = c( rep(0.0,jr-1), y2[jr,1:(n-(jr-1))]); # always n cols
    }
    y4=apply(y3,2,sum);  sum(y4); # 99.82
    y4
    # [ 1] 0.00000 0.00000 4.00000 6.40000 7.68000 8.19200
    # [ 7] 8.19200 7.86432 7.34003 6.71089 6.03980 5.36871
    # [13] 4.72446 4.12317 3.57341 3.07863 2.63883 2.25180
    # [19] 1.91403 1.62130 1.36909 1.15292 0.96845 0.81166
    # [25] 0.67884 0.56668 0.47224 0.39290 0.32641 0.27080
    # [31] 0.22438 0.18569 0.15350 0.12677 0.10458 0.08620
    # [37] 0.07099 0.05841 0.04803 0.03946
    par(mfcol=c(1,1));
    plot(0:(n-1),y4,pch=20,ylim=c(0,10),
         ylab="Desorbing Smolts",xlab="Additional Transit Days")

  #5.5 compare matrix result to convolution
    y = c(100,rep(0,89)) # length 90
    y5  =Convolve(y,st_conv) # lagged and smeared.
    lines(0:39, y5[1:40],type="l", col="blue" ) # perfect. starts day zero.
    length(y5); sum(y5); # 90 100
    y5[1:40] #
    # [ 1] 0.00000 0.00000 4.00000 6.40000 7.68000 8.19200
    # [ 7] 8.19200 7.86432 7.34003 6.71089 6.03980 5.36871
    # [13] 4.72446 4.12317 3.57341 3.07863 2.63883 2.25180
    # [19] 1.91403 1.62130 1.36909 1.15292 0.96845 0.81166
    # [25] 0.67884 0.56668 0.47224 0.39290 0.32641 0.27080
    # [31] 0.22438 0.18569 0.15350 0.12677 0.10458 0.08620
    # [37] 0.07099 0.05841 0.04803 0.03946

  # 5.6 multiple passes through storage
    # exp(t*absorb.r) of smolts never go into storage (eventually near zero).
    # same applies to desorbed smolts.
    y6= Convolve(y5,st_conv)
    y7= Convolve(y6,st_conv)
    txt.x=c(which.max(y5),which.max(y6),which.max(y7)); # modal lag 4 13 21
    txt.y=c(y5[txt.x[1]],y6[txt.x[2]],y7[txt.x[3]]);# peak 8.192 5.003 3.921
    plot (0:60, c(0,y5[1:60]),type="l",ylim=c(0,10),
          ylab="Desorbing Smolts", xlab="Additional Transit Days")
    lines(0:60,c(0,y6[1:60]))
    lines(0:60,c(0,y7[1:60]))
    lbls=paste(c("once","twice","thrice"),"in storage")
    text(txt.x,txt.y, lbls, pos=4, offset=1 )

  # 5.7 apply to storage to advection and diffusion

    y = c(100,rep(0,89)) # length 90
    y.ad=Convolve(y,ad_conv)
    y.mainstream= y.ad*exp(absorb.r* days); sum(y.mainstream) # 38.25
    y.stored= y.ad - y.mainstream; sum(y.stored)# 61.75
    y.desorb= Convolve(y.stored,in_conv); sum(y.desorb) # 61.75
    y.obs=y.desorb+  y.mainstream;sum(y.obs); sum(y.obs)
    par(mfcol=c(2,1))
    plot(days,y.ad,type="s",ylim=c(0,80),xlim=c(0,30), xlab="",ylab="")
    lines(days,    100*exp(absorb.r *days), lty="dotted") # % remaining
    FilledStep(days,y.mainstream)
    # lines(days,y.mainstream, type="s",col="blue" )
    FilledStep(days,y.desorb, colr="#FFA54F7D")
    #lines(days,y.desorb, type="s",col="red")

    plot(1,1,type="n",ylim=c(0,40),xlim=c(0,30), xlab="Arrival Day",ylab="")
    lines(days,y.obs, type="s", lwd=2)
    #FilledStep(days,y.desorb, "#FFEFD57D")
    FilledStep(days,y.mainstream, "skyblue")
    mtext("     Smolts",2,0,T)


    lines(days,cumsum(y.ad), type="s")
    lines(days,cumsum(y.in), type="s",col="red")
    lines(days,cumsum(y.out), type="s",col="blue")

