parameters {
  real<lower = 0, upper = 1> theta[1];
}
model {
  theta[2] ~ normal(0, 1);
}