# read USGS data for Okanogan River at Mallott.R

# https://waterdata.usgs.gov/nwis/dv?cb_00060=on&format=rdb&site_no=12447200&referred_module=sw&period=&begin_date=2004-01-01&end_date=2016-12-31
# skip 30 lines of headers including:
## retrieved: 2017-05-30 13:33:05 EDT       (caww01)
## Data for the following 1 site(s) are contained in this file
##    USGS 12447200 OKANOGAN RIVER AT MALOTT, WA
##            TS   parameter     statistic     Description
##        149745       00060     00003     Discharge, cubic feet per second (Mean)
# data looks like:
## USGS	12447200	2004-01-01	580	A:e  # A is approved for public, e is estimated
# turns out that "Ice" replaces flow for days in 2016 December.
# 0. SETUP ----
options(digits=5,show.error.locations=T,stringsAsFactors=F,show.signif.stars=F);
SetPar=function() par(
    oma=c(2,2,2,1),    # room for marginal titles, 2 lines
    mar = c(2,2,1,0),  # spacing between plots
    mgp = c(1,0,0),    # axis numbers close to axis
    tcl = 0.2,         # ticks inside
    xaxs = "i",yaxs = "i" # needs xlim, ylim
);

# 1. READ ----
file= "/Users/Scott/Documents/Projects/DFO - OK/flows and spills/flow Mallot WA Okanogan R 2004-2016.txt"
a= as.data.frame(scan(file, skip=30, what=list(agency="",stn="",date="",flow="", qc="")))
a[1,] # 4749 rows 5 cols
# agency      stn       date flow  qc
#   USGS 12447200 2004-01-01  580 A:e
a$flow[a$flow =="Ice"] ="NA"
b=strptime(a[,3], format="%Y-%m-%d");
# 1 KCFS is 28.316847 M^3 S^-1, so 1 kcfs is 0.028326847
OK_flow=data.frame(year=b$year+1900, doy=b$yday+1,flow=as.numeric(a$flow)*0.028326847 ); OK_flow[c(1,4749),];
#      year doy flow
# 1    2004   1 16.43
# 4749 2016 366    NA
save(OK_flow, file="/Users/Scott/Documents/Projects/DFO - OK/OK Data from R/OK_flow.Rdata")

# 2. TABLE ----
a = OK_flow[OK_flow$doy > 119 & OK_flow$doy < 141 , ] # 21 days # 273 by 3, 273/13=21.
mf=tapply(a$flow,a$year,mean);round(mf,0) # mean flow 21 days, in m^3 s^-1
#2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016
# 210  154  200  254  115   85  124  180  319  390  322  176  342 # 2009 < 1/4 of 2013
SetPar();
plot(2004:2016, mf, type="o", pch=20,ylim=c(0, 400),xlim=c(2003,2017),
     ylab="Flow (cms)", xlab="Year", main="Okanogan R. at Mallott, days 120-140")
mdf=tapply(a$flow,a$year,median);round(mdf,0)
plot(2004:2016, mdf, type="o", pch=20,ylim=c(0, 500),xlim=c(2003,2017),
     ylab="Median Flow (cms)", xlab="Year", main="Okanogan R. at Mallott, days 120-140")
OK_medianFlow=mdf
save(OK_medianFlow, file="/Users/Scott/Documents/Projects/DFO - OK/OK Data from R/OK_medianFlow.Rdata")
