---
title: "Smolt Survival 1996-2017"
author: "Scott Akenhead"
date: '2017-10-19'
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(stringsAsFactors = F);
# --- LOCAL FUNCTIONS ----
source('~/Documents/R_main/rScott/R/Simple.R')
SetPar=function() par(   # a function with one statement
    oma = c(2,2,1,1),    # room for marginal titles
    mar = c(3,3,1,0),    # spacing between plots
    mgp = c(1.5,0,0),    # axis numbers close to axis
    tcl = 0.2,           # ticks inside
    xaxs = "i",          # begs xlim=c(0,1.1*max(x))
    yaxs = "i",          # begs ylim=c(0,1.1*max(y))
    pch=20               # small points
); 
axis34=function(){axis(3,label=F);axis(4,label=F);} # just ticks, top and right
```

## Smolt Survival Data 

Data from NMFS memorandum, Zabel et al 2017 September 18, forwarded by Margot Stockwell, DFO/PBS 2017 October 19.  This is the last three columns of *Table 7. Estimated survival and standard error (s.e.) for sockeye salmon (hatchery and wild combined) from ...  Rock Island Dam tailrace to Bonneville Dam tailrace for fish originating in the upper Columbia River, 1996–2017. Note that this table represents all available data on sockeye; estimates are provided regardless of the precision, which in some years was very poor. Abbreviations: LGR– Lower Granite Dam; MCN–McNary Dam; BON–Bonneville Dam; RIS–Rock Island Dam.*  

This is from PIT tagged smolts. The text summarizes *Estimated survival in 2017 of Columbia River sockeye salmon (hatchery and wild combined) from the tailrace of Rock Island Dam to the tailrace of Bonneville Dam was 50.0% (95% CI: 15.3%, 163.5%; Table 7)*

### Read data

Thee data was extracted from PDF to Excel and reorganized by hand with means and SDs in separate columns, exported as .csv, the  read in to R as follows. Inexplicably, expanding text to columns in Excel created negative values for SDs, easily fixed. 

```{r rs, results='hold'}
f1 <- "/Users/Scott/Documents/Projects/DFO - OK/Data/from FPC/survival sockeye upper columbia 1996-2017 Table 7 Zabel 2017 September 17.csv"
if(!file.exists(f1)) stop("file not found");
raw <- read.csv(f1); 
cat("\nObtained", dim(raw)[1], "rows and",dim(raw)[2], "columns.\n")
# means are in every second column starting with second, SDs start in third column.
s.m <- raw[1:3, 2*1:23];
s.s <- raw[1:3, 1+2*1:23]; 
s.s <- -s.s # excel made these negative
yr  <- 1996:2017;
run <- c('RIS-BON', 'RIS-MCN', 'MCN-BON');
```

### Plot survivals
```{r ps, fig.height=5}
par(mfrow=c(3,1)) # stack of plots
SetPar() # tight science-y style
# par() # axis labels horizontal
for(j in 1:3){
    plot(yr, s.m[j,1:22], type="o", xlim=c(1995,2018), ylim=c(0,1.8),
         xlab="",ylab="",main=run[j], yaxt="n"); 
    axis(2, at=c(0.5,1.0),las=1); axis34(); 
    abline(h=c(0.5,1.0), col="grey80")
    for(k in 1:22){
        if(is.na(s.m[j,k]+s.s[j,k])) next # no data
        segments(yr[k], s.m[j,k]+s.s[j,k], yr[k], s.m[j,k]-s.s[j,k])
    }    
}
mtext("Year",1,outer=T); mtext("Survival",2,outer=T)
    
```

**Figure 1.** Survival estimates from PIT tags for sockeye smolts between three dams on the Columbia River. Vertical lines mark one standard deviation of the mean. Poor estimates result in survivals greater than 1, e.g. MCN-BON 1997 and 2004. 

### Table
This output is suitable for use in MS Word, using *convert text to table* with columns separated by a space. Note the last row is mean over years.

```{r tbl, results='hold'}
cat("Year",run,"\n")
yr=c(yr,"mean")
for(j in 1:23){
     cat( yr[j]," ",
     s.m[1,j],"(",s.s[1,j],") ",
     s.m[2,j],"(",s.s[2,j],") ",
     s.m[3,j],"(",s.s[3,j],")\n",sep="")
}
```

**Table 1.** Mean survival by years for sockeye smolts in the lower Columbia River.

### Simple statistics
Given imprecise estimates of survival, median and MAD are safest. A weighed mean is calculated. 


```{r ss, results='hold'}
# transpose 
s.mt=t(s.m); s.st=t(s.s)
cat(run,"\n") # header
Simple(s.mt[1:22,])
mw=numeric(3) # weighted means
for(j in 1:3) mw[j]= sum(s.mt[,j] * s.st[,j]^-2, na.rm=T)/sum(s.st[,j]^-2, na.rm=T);
cat("mw",round(mw,3),"\n", sep="  ")
```

**Table 2.** Statics for annual survival estimates. Last row *mw* is the mean weighted by  $se^{-2}$.

## Export to .csv
For subsequent use in Excel and R. Remember to say thanks to Margot.

```{r ex}
j=1:22 # exclude simple mean
survival_PIT_RIS_MCN_BON_1996_2017 = data.frame(
Year = 1996:2017, 
RIS_BON_m = s.mt[j,1],
RIS_BON_s = s.st[j,1],
RIS_MCN_m = s.mt[j,2],
RIS_MCN_s = s.st[j,2],
MCN_BON_m = s.mt[j,3],
MCN_BON_s = s.st[j,3])
names(survival_PIT_RIS_MCN_BON_1996_2017) <- c("Year","RIS_BON_m","RIS_BON_s","RIS_MCN_m","RIS_MCN_s","MCN_BON_m","MCN_BON_s")
# Simple(survival_PIT_RIS_MCN_BON_1996_2017) # check for jumbled
f2 <- "/Users/Scott/Documents/Projects/DFO - OK/OK Data from R/survival_PIT_RIS_MCN_BON_1996_2017.csv"
save(survival_PIT_RIS_MCN_BON_1996_2017, file=f2)
t(file.info(f2)[1:4]) # check file properties
```

## Conclusion
While 2017 is unusual in having the highest flows in the last 20 years (since 1997), the estimated survival of sockeye smolts (RIS-BON, 50.0%, SE=33.2), is not greater than the long-term simple mean (including 2017) of 50.5% (5.5), but it is greater than the weighted mean, 36.5% where imprecise and impossible (survival > 1) estimates are down-weighted. 

The difference in long-term survival between the two reaches is small. Median survival, a robust estimator, is nearly identicaly (0.69 RIS-MCN and 0.67 MCN-BON) given that the overall $\text{MAD}\times n^{-0.5}$ for mean survivals is 0.026 to 0.036 (robust estimate of standard error of overall mean). The weighted means suggest a difference, lower survival MCN-BON. 

These results need to be re-interpreted with attention to FTT from PIT tags which in turn is a function of MWTT and spill. Further, these estimates are not weighted by the abundance of smolts that each PIT tag represents, and because it is likely that *the early smolt gets the bird,* in that predation mortality will be less for the bulk of smolt passage than for the tails, these survival estimates will be less than population-weighted survival estimates.

The product of the reach survivals is the same as the observed survival over both reaches: $0.64\times0.586 = 0.375 \approx 0.365$
